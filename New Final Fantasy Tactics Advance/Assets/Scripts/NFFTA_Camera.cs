using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NFFTA_Camera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject selectorGO = GameObject.FindGameObjectWithTag("Player");
        float distanceFromSelector = Vector2.Distance(selectorGO.transform.position, transform.position);
        if(distanceFromSelector >= 0.65f )
        {
            Vector2 translateVector = selectorGO.transform.position - transform.position;
            translateVector.Normalize();
            transform.Translate(translateVector * Time.deltaTime);
        }
    }

    public void MoveToSelectedCharacter(Vector3 characterPosition)
    {
        Vector2 translateVector = characterPosition - transform.position;
        transform.Translate(translateVector);
    }
}
