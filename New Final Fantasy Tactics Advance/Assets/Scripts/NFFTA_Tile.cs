using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NFFTA_Tile : MonoBehaviour
{
    public enum eTileState
    {
        NORMAL,
        MOVE,
        TARGET
    }

    [SerializeField] eTileState mTileState = eTileState.NORMAL;
    public eTileState TileState { get => mTileState; set => mTileState = value; }

    [SerializeField] int mXPos;
    [SerializeField] int mYPos;

    public int XPos { get { return mXPos; } private set => mXPos = value; }
    public int YPos { get { return mYPos; } private set => mYPos = value; }
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnMouseOver()
    {
        GameObject playerControllerGO = GameObject.FindGameObjectWithTag("GameController");
        playerControllerGO.GetComponent<NFFTA_PlayerController>().Move(this);
    }

    private void OnMouseDown()
    {
        GameObject playerControllerGO = GameObject.FindGameObjectWithTag("GameController");

        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        if(spriteRenderer.enabled == true && spriteRenderer.color == Color.blue)
            playerControllerGO.GetComponent<NFFTA_PlayerController>().PerformAction(this, "MOVE");
    }

    public bool IsInTargetMode()
    {
        return mTileState == eTileState.TARGET;
    }
    // Update is called once per frame
    void Update()
    {
        onTileStateChange();
    }

    void onTileStateChange()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        switch (mTileState)
        {
            case eTileState.NORMAL:
                spriteRenderer.enabled = false;
                break;
            case eTileState.MOVE:
                spriteRenderer.enabled = true;
                spriteRenderer.color = Color.blue;
                break;
            case eTileState.TARGET:
                spriteRenderer.enabled = true;
                spriteRenderer.color = Color.green;
                break;
        }
    }

    public void setTilePosition(int xPost, int yPos)
    {
        mXPos = xPost;
        mYPos = yPos;
    }

    public void switchTileStateToMove()
    {
        switchTileState(eTileState.MOVE, Color.blue);
    }

    public void switchTileStateToTarget()
    {
        switchTileState(eTileState.TARGET, Color.green);
    }

    public void switchTileStateToNormal()
    {
        switchTileState(eTileState.NORMAL, Color.white);
    }
    public void switchTileState(eTileState tileState, Color color)
    {
        mTileState = tileState;
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        if(color == Color.white)
        {
            spriteRenderer.enabled = false;
        }
        else
        {
            spriteRenderer.enabled = true;
            spriteRenderer.color = color;
        }
        
    }
}
