using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NFFTA_Canvas : MonoBehaviour
{
    [SerializeField] GameObject mActionPanel;
    [SerializeField] GameObject mSkillPanel;

    // Start is called before the first frame update
    void Start()
    {
        mActionPanel.SetActive(false);
        mSkillPanel.SetActive(false);
    }

    public void EnableActionPanel()
    {
        mActionPanel.SetActive(true);
    }

    public void DisableActionPanel()
    {
        mActionPanel.SetActive(false);
    }

    public void OnMoveButtonClicked()
    {
        GameObject selectorGO = GameObject.FindGameObjectWithTag("Player");
        NFFTA_Selector selector = selectorGO.GetComponent<NFFTA_Selector>();
        selector.CurrentCharacter.CharacterState = NFFTA_Character.eCharacterState.MOVE;
        selector.UnlockSelector();
        mActionPanel.SetActive(false);
    }

    public void OnActionButtonClicked()
    {
        mActionPanel.SetActive(false);
        mSkillPanel.SetActive(true);
    }

    public void OnSnowBallButtonClicked()
    {
        GameObject selectorGO = GameObject.FindGameObjectWithTag("Player");
        NFFTA_Selector selector = selectorGO.GetComponent<NFFTA_Selector>();
        selector.CurrentCharacter.CharacterState = NFFTA_Character.eCharacterState.TARGET;
        selector.UnlockSelector();
        mSkillPanel.SetActive(false);
    }

    public void OnWaitButtonClicked()
    {
        mActionPanel.SetActive(false);
        GameObject selectorGO = GameObject.FindGameObjectWithTag("Player");
        NFFTA_Selector selector = selectorGO.GetComponent<NFFTA_Selector>();
        selector.CurrentCharacter.CharacterState = NFFTA_Character.eCharacterState.PASS;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
