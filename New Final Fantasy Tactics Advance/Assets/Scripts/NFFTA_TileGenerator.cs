using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEditor;
using Unity.VisualScripting;

public class NFFTA_TileGenerator : MonoBehaviour
{
    [SerializeField] GameObject mTilePrefab;
    [SerializeField] int mLevelHeight = 4;
    private Dictionary<string, NFFTA_Tile> mTileDict;
    [SerializeField] List<NFFTA_Tile> mTileList;

    public static NFFTA_TileGenerator Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        LinkTilesToDict();
    }

    private void LinkTilesToDict()
    {
        mTileDict= new Dictionary<string, NFFTA_Tile>();

        foreach(NFFTA_Tile tile in mTileList)
        {
            string tileKey = tile.XPos + ";" + tile.YPos;
            mTileDict.Add(tileKey, tile);
        }
    }

    public void CreateTiles()
    {
        int levelHeight = mLevelHeight;
        float startXPos = 0.0f;
        float currentXPos = 0;
        float currentYPos = 0;

        bool isFirstLoop = true;

        int maxRowSize = levelHeight * 2;
        int tileListSize = Mathf.RoundToInt(Mathf.Pow(maxRowSize, 2.0f));

        int YStartIndex = maxRowSize;
        int XStartIndex = maxRowSize;
        int YCurrentIndex;
        int XCurrentIndex;

        while(maxRowSize > 0)
        {
            YCurrentIndex = YStartIndex;
            XCurrentIndex = XStartIndex;
           for (int i = 0; i< maxRowSize; i++)
           {
                GameObject newTile = Instantiate(mTilePrefab);
                newTile.transform.position = new Vector3(currentXPos, currentYPos, 0.0f);
                newTile.transform.SetParent(transform);
                newTile.GetComponent<NFFTA_Tile>().setTilePosition(XCurrentIndex, YCurrentIndex);
                mTileList.Add(newTile.GetComponent<NFFTA_Tile>());
               
                if (isFirstLoop == false)
                {
                    GameObject newTile2 = Instantiate(mTilePrefab);
                    newTile2.transform.position = new Vector3(currentXPos, -currentYPos, 0.0f);
                    newTile2.transform.SetParent(transform);
                    newTile2.GetComponent<NFFTA_Tile>().setTilePosition(YCurrentIndex, XCurrentIndex);
                    mTileList.Add(newTile2.GetComponent<NFFTA_Tile>());
                }
               
                YCurrentIndex--;
                XCurrentIndex--;
                
                currentXPos += 0.3f;
           }

            isFirstLoop = false;

            XStartIndex--;
            maxRowSize--;
            currentYPos += 0.075f;
            startXPos += 0.15f;
            currentXPos = startXPos;
        }

    }

    public Vector3 onCharacterStart(int xPos, int yPos)
    {
        string tileKey = xPos+ ";" + yPos;
        return mTileDict[tileKey].transform.position;
    }

    public void onCharacterSwitchState(int xPos, int yPos, int moveScope, NFFTA_Character.eCharacterState characterState)
    {
        int startXPos = xPos - moveScope;
        int startYPos = yPos - moveScope;
        int endXPos = xPos + moveScope;
        int endYPos  = yPos + moveScope;

        for(int x = startXPos; x <= endXPos;x++)
        {
            for(int y = startYPos; y <= endYPos;y++)
            {
                string tileKey = x + ";" + y;
                if(mTileDict.ContainsKey(tileKey) == true)
                {
                    int distanceX = x - xPos;
                    int distanceY = y - yPos;
                    float tileDistance = Mathf.Sqrt(distanceX * distanceX + distanceY * distanceY);
                    if( tileDistance == moveScope ||tileDistance <= moveScope - 0.25f)
                    {
                        switchTileState(characterState, mTileDict[tileKey]);
                    }
                }
            }
        }
    }

    public void switchTileState(NFFTA_Character.eCharacterState characterState, NFFTA_Tile tile)
    {
        switch(characterState)
        {
            case NFFTA_Character.eCharacterState.MOVE:
                tile.switchTileStateToMove();
                break;
            case NFFTA_Character.eCharacterState.TARGET:
                tile.switchTileStateToTarget(); 
                break;
            case NFFTA_Character.eCharacterState.WAIT:
                tile.switchTileStateToNormal();
                break;
        }
    }

    public bool IsTarget(int xPos, int yPos)
    {
        string tileKey = xPos + ";" + yPos;
        if(mTileDict.ContainsKey(tileKey))
        {
            return mTileDict[tileKey].IsInTargetMode();
        }

        return false;
    }

}
