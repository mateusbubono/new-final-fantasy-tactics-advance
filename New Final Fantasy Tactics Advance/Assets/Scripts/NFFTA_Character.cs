using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using Unity.VisualScripting;
using UnityEngine;
using static NFFTA_Character;

public class NFFTA_Character : MonoBehaviour
{
    public enum eCharacterState
    {
        WAIT,
        MOVE,
        TARGET,
        PASS
    }

    public enum eCharacterOrientation
    {
        UP_RIGHT = 0,
        DOWN_RIGHT = 1,
        DOWN_LEFT = 2,
        UP_LEFT = 3
    }


    [SerializeField] eCharacterState mCharacterState = eCharacterState.WAIT;
    [SerializeField] int mXPos;
    [SerializeField] int mYPos;
    [SerializeField] int mMoveScope;
    [SerializeField] int mTargetScope;

    [SerializeField] GameObject[] mCharacterOrientation;
    [SerializeField]GameObject[] mTargetOrientation;
    [SerializeField] eCharacterOrientation mCurrentOrientation = eCharacterOrientation.UP_RIGHT;

    public int XPos { get => mXPos; set => mXPos = value; }
    public int YPos { get => mYPos; set => mYPos = value; }

    public eCharacterState CharacterState { get => mCharacterState;  set { mCharacterState = value;} }

    // Start is called before the first frame update
    void Start()
    {
        Vector3 startedPosition = NFFTA_TileGenerator.Instance.onCharacterStart(mXPos, mYPos);
        transform.position = startedPosition;

    }

    private void onCharacterOrientationChange()
    {
        for(int i=0; i<mCharacterOrientation.Length; i++)
        {
            int computedOrientation = (int)mCurrentOrientation;
            if (i == computedOrientation)
            {
                mCharacterOrientation[i].SetActive(true);
            }
            else
            {
                mCharacterOrientation[i].SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        onCharacterStateChange();

        onCharacterOrientationChange();

        if(mCharacterState == eCharacterState.PASS)
        {
            GameObject mousePointer = GameObject.FindGameObjectWithTag("MousePointer");
            float distanceToMouse;
            float winDistance = 0.0f;
            int orientationWinnerIndex = -1;

            for(int i = 0; i<mCharacterOrientation.Length; i++)
            {
                Vector3 vectorToMouse = mousePointer.transform.position - mCharacterOrientation[i].transform.up;
                distanceToMouse = Vector3.Distance(mousePointer.transform.position, mTargetOrientation[i].transform.position);
                if(orientationWinnerIndex == -1 || distanceToMouse < winDistance)
                {
                    //if (orientationWinnerIndex > -1)
                        //mCharacterOrientation[orientationWinnerIndex].SetActive(false);
                    orientationWinnerIndex = i;
                    winDistance = distanceToMouse;
                }
            }
            //mCharacterOrientation[orientationWinnerIndex].SetActive(true);
            mCurrentOrientation = (eCharacterOrientation)orientationWinnerIndex;

            if (Input.GetMouseButton(0))
            {
                GameObject playerControllerGO = GameObject.FindGameObjectWithTag("GameController");
                NFFTA_PlayerController playerController = playerControllerGO.GetComponent<NFFTA_PlayerController>();
                playerController.OnWaitPositionSelected();
            }
        }
    }

    private void onCharacterStateChange()
    {
        switch(mCharacterState)
        {
            case eCharacterState.MOVE:
                NFFTA_TileGenerator.Instance.onCharacterSwitchState(mXPos, mYPos, mMoveScope, mCharacterState); 
                break;
            case eCharacterState.TARGET:
                NFFTA_TileGenerator.Instance.onCharacterSwitchState(mXPos, mYPos, mTargetScope, mCharacterState);
                break;
            case eCharacterState.WAIT:
                NFFTA_TileGenerator.Instance.onCharacterSwitchState(mXPos, mYPos, mMoveScope, mCharacterState);
                break;
        }
    }


    private void OnMouseDown()
    {

        GameObject playerControllerGO = GameObject.FindGameObjectWithTag("GameController");
        NFFTA_PlayerController playerController = playerControllerGO.GetComponent<NFFTA_PlayerController>();

        if (NFFTA_TileGenerator.Instance.IsTarget(mXPos, mYPos) == true)
        {
            playerController.PerformAction(null, "ATTACK");
        }
        else
        {
            playerController.OnCharacterSelected(Input.GetMouseButtonDown(0), this);
        }
    }

    public void MoveCharacter(NFFTA_Tile tile)
    {
        mCharacterState = eCharacterState.WAIT;
        NFFTA_TileGenerator.Instance.onCharacterSwitchState(mXPos, mYPos, mMoveScope, mCharacterState);

        mXPos = tile.XPos;
        mYPos = tile.YPos;

        transform.position = tile.transform.position;

    }

    public void EndCharacterAction()
    {
        mCharacterState= eCharacterState.WAIT;
    }
}
