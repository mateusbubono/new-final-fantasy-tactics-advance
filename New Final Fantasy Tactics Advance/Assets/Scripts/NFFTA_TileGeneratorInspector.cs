using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(NFFTA_TileGenerator))]
public class NFFTA_TileGeneratorInspector : Editor
{
    public NFFTA_TileGenerator Current
    {
        get
        {
            return (NFFTA_TileGenerator)target;
        }
    }

    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();
        if (GUILayout.Button("Generate Tiles"))
        {
            Current.CreateTiles();
        }
    }
}
