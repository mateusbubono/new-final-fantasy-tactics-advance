using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NFFTA_PlayerController : MonoBehaviour
{
    [SerializeField] NFFTA_Selector mSelector;
    [SerializeField] NFFTA_Camera mCamera;
    [SerializeField] NFFTA_Canvas mCanvas;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1) && CancelCurrentAction() == false)
            OnCharacterSelected(false, null);
    }

    public void Move(NFFTA_Tile tile)
    {
        mSelector.Move(tile);
    }

    public void PerformAction(NFFTA_Tile tile, string actionType)
    {
        switch(actionType)
        {
            case "MOVE":
                mSelector.CurrentCharacter.MoveCharacter(tile);
                mSelector.LockSelector();
                mCamera.MoveToSelectedCharacter(mSelector.transform.position);
                mCanvas.EnableActionPanel();
                break;
            case "ATTACK":
                Debug.Log("ATTACK");
                mSelector.CurrentCharacter.EndCharacterAction();
                mSelector.FocusOnCurrentCharacter();
                mSelector.LockSelector();
                mCanvas.EnableActionPanel();
                break;
        }
    }

    public void OnCharacterSelected(bool isLeftMouseClicked, NFFTA_Character selectedCharacter)
    {
        if(isLeftMouseClicked)
        {
            mSelector.LockSelector();
            mCamera.MoveToSelectedCharacter(mSelector.transform.position);
            mCanvas.EnableActionPanel();
        }
        else
        {
            mSelector.UnlockSelector();
            mCanvas.DisableActionPanel();
        }
            mSelector.CurrentCharacter= selectedCharacter;
    }

    public bool CancelCurrentAction()
    {
        if(mSelector.CurrentCharacter != null &&
            mCanvas.enabled == true &&
            mSelector.CurrentCharacter.CharacterState != NFFTA_Character.eCharacterState.WAIT)
        {
            mCanvas.EnableActionPanel();
            mSelector.CurrentCharacter.CharacterState = NFFTA_Character.eCharacterState.WAIT;
            return true;
        }

        return false;
    }

    public void OnWaitPositionSelected()
    {
        mSelector.UnlockSelector();
        mSelector.CurrentCharacter.CharacterState = NFFTA_Character.eCharacterState.WAIT;
        mSelector.CurrentCharacter= null;
        mCanvas.DisableActionPanel();
    }
}
