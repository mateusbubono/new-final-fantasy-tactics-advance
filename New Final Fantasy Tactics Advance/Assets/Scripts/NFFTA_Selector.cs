using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NFFTA_Selector : MonoBehaviour
{
    [SerializeField] int mXPos;
    [SerializeField] int mYPos;
    [SerializeField] bool mIsLocked;
    [SerializeField] NFFTA_Character mCurrentCharacter;

    public NFFTA_Character CurrentCharacter { get => mCurrentCharacter; set => mCurrentCharacter = value; }

    public bool IsLocked { get => mIsLocked; private set { mIsLocked = value;} }

    public void Move(NFFTA_Tile tile)
    {
        if(mIsLocked == false)
        {
            mXPos = tile.XPos;
            mYPos = tile.YPos;

            transform.position = tile.transform.position;
        }
    }

    public void LockSelector()
    {
        mIsLocked = true;
    }

    public void UnlockSelector()
    {
        mIsLocked = false;
    }

    public void FocusOnCurrentCharacter()
    {
        if(mCurrentCharacter != null)
        {
            transform.position = mCurrentCharacter.transform.position;
            mXPos = mCurrentCharacter.XPos;
            mYPos = mCurrentCharacter.YPos;
        }
    }
}
